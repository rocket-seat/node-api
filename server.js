const express = require('express')
const mongoose = require('mongoose')
const requireDir = require('require-dir')
const cors = require('cors')

const app = express()

mongoose.connect('mongodb://localhost:27017/nodeapi', { useNewUrlParser: true })

// Captura todos os arquivos da pasta e faz o "require"
requireDir('./src/models')

app.use(express.json())
app.use(cors())
app.use('/api', require('./src/routes'))

app.listen(3001)